package br.com.primeiroprojeto.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.primeiroprojeto.conexao.Conexao;
import br.com.primeiroprojeto.modelo.Aluno;


public class AlunoDao {
		
		public int inserirAluno(Aluno aluno) throws SQLException{
			Conexao c = new Conexao();
			Connection cc = c.conectar();
			
			String consulta= "Insert into aluno(nome, matricula, data_nascimento, data_matricula) values('"+ 
			aluno.getNome()+"','"+aluno.getMatricula()+"','"+aluno.getDataNascimento()
					+"','"+aluno.getDataMatricula()+"')";
			
			Statement stm = (Statement)cc.createStatement();
			int result = stm.executeUpdate(consulta);
			
			c.desconectar();
			return result;
		}
		
	public List<Aluno> buscaTodosAlunos() throws SQLException{
		Conexao c = new Conexao();
		Connection cc = c.conectar();
		List<Aluno> alunos = new ArrayList<Aluno>();
		
		String consulta = "SELECT * FROM aluno";
		Statement stm = (Statement) cc.createStatement();
		ResultSet result = stm.executeQuery(consulta);
		
		while(result.next()){
			Aluno a = new Aluno();
			a.setNome(result.getString("nome"));
			a.setMatricula(result.getString("matricula"));
			a.setDataNascimento(result.getDate("data_nascimento"));
			a.setDataMatricula(result.getDate("data_matricula"));
			
			alunos.add(a);
		}
		
		return alunos;
		
		
		
	}
	 public List<Aluno> buscaAlunoPorNome(String nome) throws SQLException{
		 Conexao c = new Conexao();
			Connection cc = c.conectar();
			List<Aluno> alunos = new ArrayList<Aluno>();
			
			String consulta = "SELECT * FROM aluno where nome like'%"+nome+"%'";
			Statement stm = (Statement) cc.createStatement();
			ResultSet result = stm.executeQuery(consulta);
			
			while(result.next()){
				Aluno a = new Aluno();
				a.setNome(result.getString("nome"));
				a.setMatricula(result.getString("matricula"));
				a.setDataNascimento(result.getDate("data_nascimento"));
				a.setDataMatricula(result.getDate("data_matricula"));
				
				alunos.add(a);
			}
			
			return alunos;
			 
	 }
}

package br.com.primeiroprojeto.dao;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.primeiroprojeto.modelo.Prova;
import br.com.primeiroprojeto.conexao.Conexao;

public class ProvaDao {
	

	public int inserirProva(Prova prova) throws SQLException{
		Conexao c = new Conexao();
		Connection cc = c.conectar();
	
		
		String consulta = "Insert into prova(data_realizacao, descricao)values(' "
		+ prova.getData_realizacao()+ "','"+prova.getDescricao()+"')";
		
		Statement stm = (Statement) cc.createStatement();
		int result = stm.executeUpdate(consulta);
		
		c.desconectar();
		return result;
	}
	
	public List<Prova> buscarProva() throws SQLException{
		Conexao c = new Conexao();
		Connection cc = c.conectar();
		List<Prova> listProva = new ArrayList<Prova>();
		
		String consulta = "SELECT * FROM prova";
		Prova p = null;
		
		Statement stn = (Statement) cc.createStatement();
		ResultSet result = stn.executeQuery(consulta);
		
		while (result.next()){
			p = new Prova();
			p.setDescricao(result.getString("descricao"));
			p.setData_realizacao(result.getDate("data_realizacao"));
			
			listProva.add(p);
		}
		c.desconectar();
	return null;	
	}
	
	public List<Prova> BuscaTodosAlunos(){
		return null;
	}
	
}

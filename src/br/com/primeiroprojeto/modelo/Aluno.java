package br.com.primeiroprojeto.modelo;

import java.io.Serializable;
import java.util.Date;

public class Aluno implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String nome;
	private String matricula;
	private Date dataNascimento;
	private Date dataMatricula;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Date getDataMatricula() {
		return dataMatricula;
	}
	public void setDataMatricula(Date dataMatricula) {
		this.dataMatricula = dataMatricula;
	}
	
	
}

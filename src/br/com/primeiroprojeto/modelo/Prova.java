package br.com.primeiroprojeto.modelo;

import java.io.Serializable;
import java.util.Date;

public class Prova implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Date data_realizacao;
	private String descricao;
	
	
	
	public Date getData_realizacao() {
		return data_realizacao;
	}
	public void setData_realizacao(Date data_realizacao) {
		this.data_realizacao = data_realizacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	
}

package br.com.primeiroprojeto.modelo;

import java.io.Serializable;

public class Usuario implements Serializable{
 int matricula;
 String senha;
 String nome;
 
 
     public int getMatricula() {
	return matricula;
}
     public void setMatricula(int matricula) {
	 this.matricula = matricula;
}
     public String getSenha() {
	  return senha;
}
    public void setSenha(String senha) {
	this.senha = senha;
}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
 
}

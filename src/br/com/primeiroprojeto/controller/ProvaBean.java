package br.com.primeiroprojeto.controller;

import java.io.Serializable;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.primeiroprojeto.dao.ProvaDao;
import br.com.primeiroprojeto.modelo.Prova;

@ManagedBean(name = "provaBean")
@RequestScoped
public class ProvaBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	ProvaDao provaDao;
	Prova novaProva;
		
	@PostConstruct
	public void init() {
		
		provaDao = new ProvaDao();
		novaProva = new Prova();
	}

	public void salvarProva() throws SQLException {
		
		if(novaProva.getDescricao().equals("")){
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage("MensagensErro", new FacesMessage(
					"Informe a descricao da Prova: "));
			
		}else if(novaProva.getData_realizacao()==null){
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage("MensagensErro", new FacesMessage(
					"Informe a data da realiza��o da rrova"));
		}else{
			
		int conseguiuSalvar = provaDao.inserirProva(novaProva);

		if (conseguiuSalvar != 0) {
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage("menssagesSucesso", new FacesMessage(
					"Prova salva com sucesso"));
			
			novaProva = new Prova();
			
		} else {
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage("MensagensErro", new FacesMessage(
					"Prova n�o salva!"));
		}
		}
	}
	public Prova getNovaProva() {
		return novaProva;
	}
	public void setNovaProva(Prova novaProva) {
		this.novaProva = novaProva;
	
}
}

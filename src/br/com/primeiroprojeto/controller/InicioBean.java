package br.com.primeiroprojeto.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.primeiroprojeto.modelo.Usuario;

@ManagedBean(name="inicioBean")
@RequestScoped
public class InicioBean implements  Serializable{
	
	private static final long serialVersionUID = 1L;
		Usuario usuariologado;
		
		@PostConstruct
		public void init(){
			usuariologado = new Usuario();
			
			 FacesContext context = FacesContext.getCurrentInstance();
    		 HttpSession session = (HttpSession) context
    				 .getExternalContext().getSession(false);
    		usuariologado = (Usuario)session.getAttribute("user");
		}

		public Usuario getUsuariologado() {
			return usuariologado;
		}

		public void setUsuariologado(Usuario usuariologado) {
			this.usuariologado = usuariologado;
		}
		public String logaut(){
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context
					.getExternalContext().getSession(false);
			session.removeAttribute("usuario");
			
			return "/index";
			}
	}



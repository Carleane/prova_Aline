package br.com.primeiroprojeto.controller;

import java.io.Serializable;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.primeiroprojeto.dao.AlunoDao;
import br.com.primeiroprojeto.modelo.Aluno;

@ManagedBean(name="alunoBean")
@RequestScoped
public class AlunoBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	Aluno novoAluno;
	AlunoDao alunoDao;
	
	@PostConstruct
	public void init(){
		alunoDao = new AlunoDao();
		novoAluno = new Aluno();
		
	}
	public void salvarAluno() throws SQLException{
		if(novoAluno.getNome().equals("")){
			FacesContext ctx = FacesContext.getCurrentInstance();
	   		 ctx.addMessage("mensagensErro", new FacesMessage(
	   				 "Informe o nome do aluno"));
	   		 
		}else if(novoAluno.getMatricula().equals("")){
			FacesContext ctx = FacesContext.getCurrentInstance();
	   		 ctx.addMessage("mensagensErro", new FacesMessage(
	   				 "informe a matricula do aluno"));
			
		}else if(novoAluno.getDataNascimento()== null){	
			FacesContext ctx = FacesContext.getCurrentInstance();
	   		 ctx.addMessage("mensagensErro", new FacesMessage(
	   				 "Informe a data de nascimento do aluno"));
			
		}else if(novoAluno.getDataMatricula()== null){
			FacesContext ctx = FacesContext.getCurrentInstance();
	   		 ctx.addMessage("mensagensErro", new FacesMessage(
	   				 "Informe a data da matricula do aluno"));
			
		}else{
		
		int conseguiuSalvar = alunoDao.inserirAluno(novoAluno);
		
		if(conseguiuSalvar!=0){
			FacesContext ctx = FacesContext.getCurrentInstance();
   		 ctx.addMessage("mensagensErro", new FacesMessage(
   				 "Aluno salvo com sucesso"));	 
		}else{
			FacesContext ctx = FacesContext.getCurrentInstance();
   		 ctx.addMessage("mensagensErro", new FacesMessage(
   				 "Erro ao salvar"));	 
		}
		}
		
		}
	public Aluno getNovoAluno() {
		return novoAluno;
	}

	public void setNovoAluno(Aluno novoAluno) {
		this.novoAluno = novoAluno;
	}
	
	

}

package br.com.primeiroprojeto.conexao;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	private Connection con = null;
	
	public Statement stmt;
	
	// para ter acesso com o banco de dados que � a sql
	public Connection conectar(){
		
		String endereco = "jdbc:postgresql://localhost:5432/BD_Web";
		String usuario = "postgres";
		String senha = "admin";
		
		 try {
			Class.forName("org.postgresql.Driver");
			
			con = DriverManager.getConnection(endereco,usuario, senha);
			//con = DriverManager.;
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return con;
	}
	// realiza a desconecao com o banco 
	public void desconectar(){
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
